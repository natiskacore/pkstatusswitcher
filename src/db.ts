import path from "path";
import fs from "fs";

export default class Config {
  path: string = "";
  constructor() {
    this.path = path.join(__dirname, "..", "config.json");
    if (!fs.existsSync(this.path)) {
      fs.writeFileSync(this.path, JSON.stringify({}));
    }
  }

  get(key: string) {
    const table = JSON.parse(fs.readFileSync(this.path).toString());
    return table[key];
  }

  set(key: string, value: any) {
    const table = JSON.parse(fs.readFileSync(this.path).toString());
    table[key] = value;

    fs.writeFileSync(
      this.path,
      JSON.stringify(
        table,
        (k, v) => (Array.isArray(v) ? v.filter((e) => e !== null) : v),
        2
      )
    );
  }
}
