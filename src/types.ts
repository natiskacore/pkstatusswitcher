export interface SystemPrivacy {
  description_privacy?: string;
  pronoun_privacy?: string;
  member_list_privacy?: string;
  group_list_privacy?: string;
  front_privacy?: string;
  front_history_privacy?: string;
}

export interface System {
  id: string;
  uuid: string;
  name?: string;
  description?: string;
  tag?: string;
  pronouns?: string;
  avatar_url?: string;
  banner?: string;
  color?: string;
  created?: string;
  privacy?: SystemPrivacy;
}

export interface ProxyTag {
  prefix?: string;
  suffix?: string;
}

export interface MemberPrivacy {
  visibility?: string;
  name_privacy?: string;
  description_privacy?: string;
  birthday_privacy?: string;
  pronoun_privacy?: string;
  avatar_privacy?: string;
  metadata_privacy?: string;
}

export interface Member {
  id: string;
  uuid: string;
  name: string;
  display_name?: string;
  color?: string;
  birthday?: string;
  pronouns?: string;
  avatar_url?: string;
  webhook_avatar_url?: string;
  banner?: string;
  description?: string;
  created?: string;
  proxy_tags: ProxyTag[];
  keep_proxy: boolean;
  tts: boolean;
  autoproxy_enabled?: boolean;
  message_count?: number;
  last_message_timestamp?: string;
  privacy?: MemberPrivacy;
}

export interface GroupPrivacy {
  visibility?: string;
  description_privacy?: string;
  icon_privacy?: string;
  list_privacy?: string;
  metadata_privacy?: string;
}

export interface Group {
  id: string;
  uuid: string;
  name: string;
  display_name?: string;
  description?: string;
  icon?: string;
  banner?: string;
  color?: string;
  privacy?: GroupPrivacy;
}

export interface Switch {
  id: string;
  timestamp: string;
  members: string[];
}

export interface Message {
  id: string;
  timestamp: string;
  original: string;
  sender: string;
  channel: string;
  guild: string;
  system?: System;
  member?: Member;
}

export interface SystemSettings {
  timezone: string;
  pings_enabled: boolean;
  latch_timeout?: number;
  member_default_private: boolean;
  group_default_private: boolean;
  show_private_info: boolean;
  member_limit: number;
  group_limit: number;
}

export interface SystemGuildSettings {
  guild_id?: string;
  proxying_enabled: boolean;
  tag?: string;
  tag_enabled: boolean;
}

export type AutoproxyMode = "off" | "front" | "latch" | "member";
export interface AutoproxySettings {
  autoproxy_mode: AutoproxyMode;
  autoproxy_member?: string;
  last_latch_timestamp?: string;
}

export interface MemberGuildSettings {
  guild_id: string;
  display_name?: string;
  avatar_url?: string;
  keep_proxy?: boolean;
}

export interface SimplyPluralSystem {
  content: {
    uid: string;
    fields: any;
    isAsystem: boolean;
    lastOperationTime: number;
    username: string;
    avatarUrl: string;
    color: string;
    desc: string;
    frame: any;
    supportDescMarkdown: boolean;
  };
  exists: boolean;
  id: string;
}

export interface SimplyPluralMember {
  content: {
    avatarUrl: string;
    avatarUuid: string;
    color: string;
    desc: string;
    frame: any;
    lastOperationTime: number;
    name: string;
    pkId: string;
    preventTrusted: boolean;
    preventsFrontNotifs: boolean;
    private: boolean;
    pronouns: string;
    supportDescMarkdown: string;
    uid: string;
  };
  exists: boolean;
  id: string;
}

export interface SimplyPluralFronter {
  content: {
    custom: boolean;
    startTime: number;
    live: boolean;
    member: string;
    endTime?: number;
    uid: string;
    lastOperationTime: number;
  };
  exists: boolean;
  id: string;
}
