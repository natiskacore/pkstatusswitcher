import {
  Client,
  GatewayIntentBits,
  EmbedBuilder,
  TextChannel,
  ActivityType,
} from "discord.js";
import axios from "axios";
import Config from "./db";
import {
  Member,
  SimplyPluralFronter,
  SimplyPluralMember,
  SimplyPluralSystem,
} from "./types";

axios.defaults.validateStatus = function () {
  return true;
};

const config = new Config();
const required = ["token", "pkToken", "userId", "logChannel"];

var setupFailed: boolean = false;

for (const requiredItem of required) {
  if (!config.get(requiredItem) || config.get(requiredItem) == "") {
    setupFailed = true;
    config.set(requiredItem, "");
  }
}

if (setupFailed) {
  console.log(
    `pks: please add the required credentials to your config.json file! this bot will not work otherwise :(`
  );
}

const client = new Client({
  intents: [
    GatewayIntentBits.AutoModerationConfiguration,
    GatewayIntentBits.AutoModerationExecution,
    GatewayIntentBits.DirectMessageReactions,
    GatewayIntentBits.DirectMessageTyping,
    GatewayIntentBits.DirectMessages,
    GatewayIntentBits.GuildEmojisAndStickers,
    GatewayIntentBits.GuildIntegrations,
    GatewayIntentBits.GuildInvites,
    GatewayIntentBits.GuildMembers,
    GatewayIntentBits.GuildMessageReactions,
    GatewayIntentBits.GuildMessageTyping,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.GuildModeration,
    GatewayIntentBits.GuildPresences,
    GatewayIntentBits.GuildScheduledEvents,
    GatewayIntentBits.GuildVoiceStates,
    GatewayIntentBits.GuildWebhooks,
    GatewayIntentBits.Guilds,
    GatewayIntentBits.MessageContent,
  ],
});

const filterNullValues = (obj: any) =>
  Object.fromEntries(
    Object.entries(obj).filter(([_, value]) => value !== null)
  );

var spSystem: SimplyPluralSystem = {} as SimplyPluralSystem;

const members: Member[] = [];
const spMembers: SimplyPluralMember[] = [];

client.on("ready", async () => {
  console.log(`Logged in as ${client.user!.tag}!`);

  const pluralKitRq = await axios.get(
    "https://api.pluralkit.me/v2/systems/@me/members",
    {
      headers: {
        Accept: "application/json",
        Authorization: config.get("pkToken"),
      },
    }
  );

  for (const member of pluralKitRq.data as Member[]) {
    members.push(member);
  }

  if (config.get("spToken")) {
    const { data: me } = await axios.get(`https://api.apparyllis.com/v1/me`, {
      headers: {
        Accept: "application/json",
        Authorization: config.get("spToken"),
      },
    });

    spSystem = me as SimplyPluralSystem;

    const simplyPluralRq = await axios.get(
      `https://api.apparyllis.com/v1/members/${spSystem.id}`,
      {
        headers: {
          Accept: "application/json",
          Authorization: config.get("spToken"),
        },
      }
    );

    for (const spMember of simplyPluralRq.data as SimplyPluralMember[]) {
      spMembers.push(spMember);
    }
  }
});

client.on("presenceUpdate", async (old, presence) => {
  if (!presence.userId == config.get("userId")) return;
  if (presence.activities[0] && presence.activities[0].emoji) {
    // what is this fucking code? this is horrible. - kera
    if (old && old.activities[0] && old.activities[0].emoji) {
      if (
        old.activities[0].emoji!.name! == presence.activities[0].emoji!.name!
      ) {
        return;
      }
    }

    const memberMap = config.get("memberMap") || {};
    const emojiId = presence.activities[0].emoji!.name!;

    if (memberMap[emojiId]) {
      const member = members.find((member) => member.id == memberMap[emojiId])!;
      const spMember = spMembers.find(
        (spMember) => spMember.content.pkId === member.id
      );

      await axios.post(
        "https://api.pluralkit.me/v2/systems/@me/switches",
        {
          members: [memberMap[emojiId]],
        },
        {
          headers: {
            Accept: "application/json",
            Authorization: config.get("pkToken"),
          },
        }
      );

      if (spMember) {
        if (old && old.activities[0] && old.activities[0].emoji) {
          const oldEmojiId = old.activities[0].emoji!.name!;
          const oldMember = members.find(
            (member) => member.id == memberMap[oldEmojiId]
          )!;
          const oldSpMember = spMembers.find(
            (member) => member.content.pkId === oldMember.id
          );

          const currentFronters: SimplyPluralFronter[] = (
            await axios.get(`https://api.apparyllis.com/v1/fronters`, {
              headers: {
                Accept: "application/json",
                Authorization: config.get("spToken"),
              },
            })
          ).data;

          if (
            oldSpMember &&
            currentFronters.find(
              (fronter) => fronter.content.member === oldSpMember.id
            )
          ) {
            const fronter: SimplyPluralFronter = currentFronters.find(
              (fronter) => fronter.content.member === oldSpMember.id
            )!;

            await axios.patch(
              `https://api.apparyllis.com/v1/frontHistory/${fronter.id}`,
              {
                endTime: parseInt(Date.now().toFixed()),
                live: false,
              },
              {
                headers: {
                  Accept: "application/json",
                  Authorization: config.get("spToken"),
                },
              }
            );
          }

          const shit = await axios.post(
            `https://api.apparyllis.com/v1/frontHistory`,
            {
              custom: false,
              startTime: parseInt(Date.now().toFixed()),
              live: true,
              member: spMember.id,
            },
            {
              headers: {
                Accept: "application/json",
                Authorization: config.get("spToken"),
              },
            }
          );

          console.log(shit.data);
        }
      }

      const embed = new EmbedBuilder()
        .setColor(0xaa7cff)
        .setTitle(`${emojiId} Registered switch`)
        .setDescription(
          `Current fronter is now "${member.display_name || member.name}".`
        )
        .setTimestamp()
        .setFooter({ text: "plkswtchr" });

      const channel = (await client.channels.fetch(
        config.get("logChannel")
      ))! as TextChannel;
      channel.send({ content: `<@${config.get("userId")}>`, embeds: [embed] });
    }
  }
});

client.on("messageCreate", async (message) => {
  if (message.author.bot) return;
  if (!message.content.startsWith("pks")) return;
  const args = message.content.split(" ");
  args.shift();

  if (args[0].startsWith("s")) {
    // our shitty command handler, woohoo!
    if (args[1].startsWith("a")) {
      if (!(args[2] && args[3])) {
        message.reply(`pks: no member and/or emoji!`);
        return;
      }

      const member = args[2];
      const emoji = args[3];

      const memberMap = config.get("memberMap") || {};
      if (!members.find((m) => m.id == member)) {
        const res = members.find((m) => m.name == member);
        if (res) {
          memberMap[emoji] = res.id;
        }
      } else {
        memberMap[emoji] = member;
      }

      config.set("memberMap", filterNullValues(memberMap));

      const realMember = (members.find((m) => m.id == member) ||
        members.find((m) => m.name == member))!;

      const embed = new EmbedBuilder()
        .setColor(0xbbffdc)
        .setTitle("Added switch")
        .setDescription(
          `The emoji ${emoji} will be used to proxy for member "${
            realMember.display_name || realMember.name
          }".`
        )
        .setTimestamp()
        .setFooter({ text: "plkswtchr" });

      message.reply({ embeds: [embed] });
    } else if (args[1].startsWith("r")) {
      if (!args[2]) {
        message.reply(`pks: no member!`);
        return;
      }

      const member = args[2];

      const memberMap = config.get("memberMap") || {};
      if (!members.find((m) => m.id == member)) {
        const res = members.find((m) => m.name == member);
        if (res) {
          if (
            Object.entries(memberMap).find(([key, value]) => value == res.id)
          ) {
            const [key] = Object.entries(memberMap).find(
              ([key, value]) => value == res.id
            )!;
            memberMap[key] = null;
          }
        }
      } else {
        if (Object.entries(memberMap).find(([key, value]) => value == member)) {
          const [key] = Object.entries(memberMap).find(
            ([key, value]) => value == member
          )!;
          memberMap[key] = null;
        }
      }

      const realMember = (members.find((m) => m.id == member) ||
        members.find((m) => m.name == member))!;

      config.set("memberMap", filterNullValues(memberMap));

      const embed = new EmbedBuilder()
        .setColor(0xff5151)
        .setTitle("Removed switch")
        .setDescription(
          `Member "${
            realMember.display_name || realMember.name
          }" will no longer be proxied.`
        )
        .setTimestamp()
        .setFooter({ text: "plkswtchr" });

      message.reply({ embeds: [embed] });
    }
  } else if (args[0].startsWith("c")) {
    if (args[1].startsWith("sp")) {
      if (!args[2]) {
        message.reply(`pks: you didn't include your Simply Plural token!`);
        return;
      }

      const token = args[2];
      config.set("spToken", token);

      const embed = new EmbedBuilder()
        .setColor(0xbbffdc)
        .setTitle("Registered Simply Plural")
        .setDescription(
          `You have been logged into Simply Plural! When you restart the bot, it will automatically sync switches between PluralKit and Simply Plural.`
        )
        .setTimestamp()
        .setFooter({ text: "plkswtchr" });

      message.reply({ embeds: [embed] });
    }
  }
});

if (!config.get("token")) {
  console.log(`pks: no token in db!`);
} else {
  client.login(config.get("token"));
}
