# PluralKit Status Switcher

A tool for plural systems to keep track of who's fronting via Discord status. Supports Simply Plural syncing! (must have PluralKit integration or else it won't work, sadly).

Rewrite of [lostkagamine/StatusSwitcher](https://github.com/lostkagamine/StatusSwitcher).

Requires Node and Yarn to run. Run `yarn` to install the dependencies, and then run `yarn start` to initially create the database.

## How To Use

### Add Switch

`pks sw[itch] a[dd] <member name/id> <emoji>`

### Remove Switch

`pks sw[itch] r[emove] <member name/id>`

## Config

In `config.json` in the root of the project's directory, there are 4 mandatory fields you need to fill out.

• `token`: Your Discord bot token. This should be pretty obvious, but you need to get this from [discord.dev](https://discord.dev). You also need to enable the Presence, Server Members and Message Content intents in the Bot section.

![Discord](images/discord.png)

• `pkToken`: This can simply be got by running `pk;token`. You should recieve a DM from PluralKit with the token, just paste that into this field.

• `userId`: Your Discord user ID. In Settings > Advanced, turn on Developer Mode. Right click on your user and click "Copy User ID". Paste that in here. Easy!

And, finally:

• `logChannel`: With Developer Mode enabled, create a text channel in your server with the bot invited. This doesn't really matter as long as the bot has permissions to the channel. Just copy that ID, and paste it in here. You're done! (for the most part).

## Simply Plural

Here's the fun part! Open your Settings in Simply Plural.

![Settings](images/settings.png)

If you don't have your system imported from PluralKit, this won't work. You can do this by going into Integrations and clicking on PluralKit.

If you do have this step completed, go to your Account Settings.

![Account](images/account.png)

Go to the Tokens section and click Add Token. Make sure all of these permissions are checked - this is necessary for the bot to run.

![Add Token](images/addtoken.png)

Once you've done that, copy the token from the page.

![Tokens](images/tokens.png)

Great! Now you're going to want to run `pks c[onfig] sp <your copied token>`. This will register the token into the database!

![Command](images/command.png)

When you restart the bot, the Simply Plural integration will work!

## Credits

**[lostkagamine](https://github.com/lostkagamine)** - Original idea <br />
**[Amaryllis](https://github.com/Cellaryllis)** - API support <br />
